<?php
// Нужные данные из сформированного массива помещений:
    require_once $_SERVER['DOCUMENT_ROOT'].'/Build.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HTMLWriterApartement.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HTMLWriterHome.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HTMLWriterHotelRoom.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Homework hillel #9 | OOP PHP">
    <title>HW_9_Moroz</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
<!-- Заголовок ДЗ -->
<div class="row container-fluid display-6">
</div>
<!-- вывод таблицы на главной -->
<div class ="container">
    <div class="row d-flex justify-content-center">
        <div class="col-8  display-8">
        <H1>Hillel Student Moroz HW#9 OOP PHP</H1>
        <H3>Premises for rent</H3>
            <table class="table  table-borderless">
                <tr>
                    <th>Title Top</th>
                    <th>Type of The House</th>
                    <th>Title Price</th>
                    <th>Link</th>
                </tr>
                <?php // получаем ключ обьектов для передачи в отдельный файл
                foreach ($houseObjects as $key => $house):?>
                <!-- Используем врайтер для записи в таблицу -->
        <?php // выбираем апартаменты...
    $my_classes = get_class($house); // тут вызываем функцию проверки названия класса
        if
            ($my_classes === "Apartement"): // проверяем класс, обычный if... 
            $writer = new HTMLWriterApartement (); // работает метод и получает данные
            echo $writer->write($house); // выводим результат работы метода
        endif;
        if 
            ($my_classes === 'Home'):
            $writer = new HTMLWriterHome();
            echo $writer->write($house);
        endif;
        if 
            ($my_classes === 'HotelRoom'):
            $writer = new HTMLWriterHotelRoom();
            echo $writer->write($house);
        endif;?>
                <!-- добавим ссылку для перехода-->
                <td><a href="/HTMLLinkPage.php?Object=<?=(string)$key?>" class="btn btn-primary" tabindex="-1" role="button" aria-disabled="true">Show More</a></td>
                </tr>
        <?php endforeach; 
        // конец вывода таблицы на экран
        ?>  
             </table>
       </div>
    </div>
</div>




<!-- Footer-->
<footer class="container-fluid table-dark col-12">
    <div class="row">
        <div class="footer bg-secondary">
                <p class="footer-text display-3 m-5">&copy; Hillel Student Moroz 2019-2021</p>
        </div>
    </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>