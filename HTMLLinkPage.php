<?php
// тут HTML разметка, здесь отображается подробная информация по ссылке
    require_once $_SERVER['DOCUMENT_ROOT'].'/getSummaryLine.php';
// тут данные всех помещений
    require_once $_SERVER['DOCUMENT_ROOT'].'/Build.php';
// проверка
            if(!empty($_GET['Object']) || $_GET['Object'] == 0){
        // получен айди объекта из цикла
            $id = $_GET['Object'];
            $house = $houseObjects[$id];
                }else{
                        header('Location:/');
                    die;
                }
            $Summary = new SummaryLine();
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Homework hillel #9 | OOP PHP">
    <title>HW_9_Moroz</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
<div class ="container">
    <div class="row d-flex justify-content-center">
        <div class="col-8">
        <H1>Hillel Student Moroz HW#9 OOP PHP</H1>
        <H3>Premises for rent</H3>
<?php 
            // Выводим нумерованный список
            echo $Summary->GetSummaryLine($house);?>
        </div>
    </div>
</div>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>