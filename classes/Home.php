<?php
// создаем класс для дома
    class Home
    {
        public $title = 'название';
        public $type = 'тип';
        public $adress = 'адрес';
        public $price = 0;
        public $description = 'описание';
        public $roomsAmount = 'количество комнат';
// создаем метод для класса
        public function __construct($title,
                                    $type,
                                    $adress,
                                    $price,
                                    $description,
                                    $roomsAmount) 
        {
            $this-> title = $title;
            $this-> type = $type;
            $this-> adress = $adress;
            $this-> price = $price;
            $this-> description = $description;
            $this-> roomsAmount = $roomsAmount;
        }
    };
?>