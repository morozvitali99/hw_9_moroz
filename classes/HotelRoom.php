<?php
// создаем класс для отеля
    class HotelRoom
    {
        public $title = 'название';
        public $type = 'тип';
        public $adress = 'адрес';
        public $price = 0;
        public $description = 'описание';
        public $roomsNumber = 'номер в отеле';
// создаем метод для класса
        public function __construct($title,
                                    $type,
                                    $adress,
                                    $price,
                                    $description,
                                    $roomsNumber)
        {
            $this-> title = $title;
            $this-> type = $type;
            $this-> adress = $adress;
            $this-> price = $price;
            $this-> description = $description;
            $this-> roomsNumber = $roomsNumber;
        }
    };
?>