<?php
// массив исходных данных
$houseArray = [
        [
            'title' => 'Andersen apartment',
            'type' => 'apartment',
            'adress' => 'Ritscher Str., 21706 Andersen, Germany',
            'price' => '800$',
            'decription' => 'The oldest part of the house is a medieval tower housed in a rustic farmhouse that is today a restaurant',
            'kitchen' => (bool) false
        ],
        [ 
            'title' => 'House Edoardo Amaldi',
            'type' => 'house',
            'adress' => 'Via Edoardo Amaldi, 215, 00134 Roma RM, Italy',
            'price' => '500$',
            'decription' => 'There are mountain lodges with Michelin-starred restaurants, aristocratic townhouses with museum-standard collections of art and antiques',
            'roomsAmount' => '4 Rooms'
        ],
        [
            'title' => "Hotel Lucia",
            'type' => 'hotel room',
            'adress' => 'Via S. Lucia, 1, 00030 Colonna RM, Italy',
            'price' => '1300$',
            'decription' => 'Rooms feature local walnut and olive floors and Italian furnishings. Stone and pink marble bathrooms have  bathtub and shower with Lefay Spa toiletries. All rooms have lake views',
            'roomsNumber' => 'Number #1'
        ],
        [
            'title' => 'Biss apartment',
            'type' => 'apartment',
            'adress' => '27 Mostyn Ave, Wembley HA9 8AY, united kingdom',
            'price' => '700$',
            'decription' => 'the hotel is decorated in the Art Nouveau style with lavishly appointed fabrics, period furniture and antique paintings and prints',
            'kitchen' => (bool) true
        ],
        [
            'title' => 'Hotel Victoria',
            'type' => 'hotel room',
            'adress' => '10 Chichester Rd, Southend-on-Sea SS2 5BQ, United Kingdom',
            'price' => '580$',
            'decription' => 'Grand and traditional, calm and elegant',
            'roomsNumber' => 'Number #100'
        ],
        [
            'title' => 'Apartment Le Solei',
            'type' => 'apartment',
            'adress' => '81 Avenue Amiral de Tourville, 62520 Le Touquet-Paris-Plage, France',
            'price' => '300$',
            'decription' => 'The apartment is set on a hillside among gorgeous tiered grounds shaded by olive trees',
            'kitchen' => (bool) true
        ],
        [
            'title' => 'House Chateau de La Roi',
            'type' => 'house',
            'adress' => '3 Rue Alsace Lorraine, 34140 Mèze, France',
            'price' => '1000$',
            'decription' => 'The entrance hall has coffered ceilings and original 19th-century carved wooden benches, while the library has a wood-burning fireplace and shelves lined with classic works of literature',
            'roomsAmount' => '20 Rooms'
        ],
        [
            'title' => 'Hotel Hellerup',
            'type' => 'hotel room',
            'adress' => 'Hans Jensens Vej 16-36, 2900 Hellerup, Denmark',
            'price' => '600$',
            'decription' => 'The hotel is located in a former convent dating from circa 13th century. Original fresco details are visible throughout the villa, both in the communal areas and in some of the rooms',
            'roomsNumber' => 'Number #19'
        ],
        [
            'title' => 'Greve house',
            'type' => 'hotel room',
            'adress' => 'Rytterbakken 25, 2670 Greve, Denmark',
            'price' => '700$',
            'decription' => 'With only seven rooms and five suites, the atmosphere is intimate and exclusive',
            'roomsNumber' => 'Number #10'
        ],
        [
            'title' => 'Barrakuda house',
            'type' => 'house',
            'adress' => 'Kronskamper Weg 13, 19306 Neustadt-Glewe, Germany',
            'price' => '900$',
            'decription' => 'Formerly a private residence, the opulent interiors feature stuccos, chandeliers and antiques',
            'roomsAmount' => '3 Rooms'
        ]
        ];        
?>
 