<?php
// Нужны все классы помещений (3) и файл исходных данных
    require_once $_SERVER['DOCUMENT_ROOT'].'/house_data.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Apartement.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HotelRoom.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Home.php';
// тут формируем каталог всех доступных помещений
    $houseObjects = [];
        foreach ($houseArray as $house):
// Добавляем экземпляр для каждого из вариантов
            if ($house['roomsAmount']!=NULL)
        {
            $houseObjects[] = new Home     ($house['title'],
                                            $house['type'],
                                            $house['adress'],
                                            $house['price'],
                                            $house['decription'],
                                            $house['roomsAmount']);
        }
// проверяем существование значений для каждого из вариантов
        if ($house['roomsNumber']!=NULL)
        {
            $houseObjects[] = new HotelRoom($house['title'],
                                            $house['type'],
                                            $house['adress'],
                                            $house['price'],
                                            $house['decription'],
                                            $house['roomsNumber']);
        }
                if (isset($house['kitchen']))
        {
            $houseObjects[] = new Apartement($house['title'],
                                            $house['type'],
                                            $house['adress'],
                                            $house['price'],
                                            $house['decription'],
                                            $house['kitchen']);
        }
        endforeach;
// Закончили формирование каталога
    ?>